#include <iostream>
#include "caneta.hpp"

using namespace std;

Caneta::Caneta(){
   cout << "Construtor da Classe Caneta" << endl;
   nome = "";
   cor = "";
   marca = "";
   preco = 0.0f;
}

Caneta::~Caneta(){
   cout << "Destruindo o objeto " << getNome() << endl;

}

string Caneta::getNome(){
   return nome;
}

void Caneta::setNome(string nome){
   this->nome = nome;
}

string Caneta::getCor(){
   return cor;
}
void Caneta::setCor(string cor){
   this->cor = cor;  
}

string Caneta::getMarca(){
   return marca;
}
void Caneta::setMarca(string marca){
   this->marca = marca;
}

float Caneta::getPreco(){
   return preco;
}
void Caneta::setPreco(float preco) {
   this->preco = preco;
}

void Caneta::escrever(float comprimento) {
   std::cout << "Caneta escrevendo" << std::endl;    
}
void Caneta::destampar(){
   std::cout << "Caneta destampada" << std::endl;
}



